# batsutils

Miscellaneous custom code for BATS Improv.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## batsutils.php

Contains custom pageRun and buildForm code

## CiviRules Custom Actions

These live in batsutils/CRM/Batsutils

### Process Donation Request

We offer 4 free tickets to shows to any California nonprofits who want to request a donation to their fundraising events. Their EIN (tax ID number) is validated against the IRS database of nonprofits via webform validation. If the webform is successfully submitted, the request is assumed to be valid and this method sends out a PDF containing 4 codes which can be used to register for a show.

PLEASE ENSURE THAT MESSAGE TEMPLATES ARE UPDATED IF THE LOCATION OF FREE PASS SIGNUP CHANGES


## Custom Templates

Customizing appearance of website.


