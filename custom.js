dataLayer = [{
    'civiEventType': CRM.vars.analytics.civiEventType,
    'civiEventIsOnline': CRM.vars.analytics.civiEventIsOnline,
    'civiEventTitle': CRM.vars.analytics.civiEventTitle,
    'civiEventId': CRM.vars.analytics.civiEventId,
    'civiEventTimeAway': CRM.vars.analytics.civiEventTimeAway,
    'civiUserId': CRM.vars.analytics.civiUserId,
    'civiUserLoggedIn': CRM.vars.analytics.civiUserLoggedIn,
    'civiUserAlreadyRegistered': CRM.vars.analytics.civiUserAlreadyRegistered
}];
