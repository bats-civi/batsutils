<?php

require_once 'batsutils.civix.php';
// phpcs:disable
use CRM_Batsutils_ExtensionUtil as E;
// phpcs:enable

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function batsutils_civicrm_config(&$config) {
  _batsutils_civix_civicrm_config($config);

  $smarty = CRM_Core_Smarty::singleton();
  // allow the explode() php function to be used as a "modifier" in Smarty templates
  array_push($smarty->security_settings['MODIFIER_FUNCS'], 'explode');
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function batsutils_civicrm_xmlMenu(&$files) {
  _batsutils_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function batsutils_civicrm_install() {
  _batsutils_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function batsutils_civicrm_postInstall() {
  _batsutils_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function batsutils_civicrm_uninstall() {
  _batsutils_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function batsutils_civicrm_enable() {
  _batsutils_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function batsutils_civicrm_disable() {
  _batsutils_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function batsutils_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _batsutils_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function batsutils_civicrm_managed(&$entities) {
  _batsutils_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_caseTypes
 */
function batsutils_civicrm_caseTypes(&$caseTypes) {
  _batsutils_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function batsutils_civicrm_angularModules(&$angularModules) {
  _batsutils_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function batsutils_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _batsutils_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function batsutils_civicrm_entityTypes(&$entityTypes) {
  _batsutils_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_thems().
 */
function batsutils_civicrm_themes(&$themes) {
  _batsutils_civix_civicrm_themes($themes);
}

function var_dump_ret($mixed = null) {
  ob_start();
  var_dump($mixed);
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
}

function fetchParticipantsWithPreferredName($eventId, $roleName) {
  $result = civicrm_api3('Participant', 'get', [
    'sequential' => 1,
    'event_id' => $eventId,
    'role_id' => $roleName,
    'status_id' => ["Attended", "Registered"],
    'options' => ['sort' => "sort_name"],
    'api.Contact.getvalue' => ['return' => "custom_200"],
  ]);
  $performers = $result['values'];
  foreach ($performers as $key=>$performer) {
    // put logic for preferred name here
    if ($performer['api.Contact.getvalue'] != "") {
      $performers[$key]['preferred_name'] = $performer['api.Contact.getvalue'];
    } else {
      $performers[$key]['preferred_name'] = $performer['display_name'];
    }
  }
  return $performers;
}


/**
 * Customize various forms.
 */
function batsutils_civicrm_buildForm($formName, &$form) {
    //\CRM_Core_Error::debug_log_message("Form is $formName ID is {$form->getVar("_id")}");
    //\CRM_Core_Error::debug_var("form", $form);

    $defaults = $form->getVar("_defaults");
    $templatePath = realpath(dirname(__FILE__)."/templates");

    // add custom JS for analytics
    Civi::resources()->addScriptFile('batsutils', 'custom.js', 10, 'html-header');
    $session = CRM_Core_Session::singleton();
    $userId = $session->get('userID');
    Civi::resources()->addVars('analytics', [
        'civiUserId' => $userId,
        'civiUserLoggedIn' => isset($userId)
    ]);

    // customize 2021 Monthly Campaign
    if (($formName == 'CRM_Contribute_Form_Contribution_Main') and ($form->getVar("_id") == 10)) {
        $defaults['installments'] = 6;
        $defaults['is_recur'] = 1;
        if ($_GET['aid'] != NULL) {
            $defaults['custom_195'] = $_GET['aid'];
        }

        // apply JS to hide the custom post profile -- we just want it there to invisibly pass Asker ID
        CRM_Core_Region::instance('page-body')->add(array(
            'template' => "{$templatePath}/ContributionForm10.tpl"
        ));
    }

    // customize Perf Lab membership
    if (($formName == 'CRM_Contribute_Form_Contribution_Main') and ($form->getVar("_id") == 15)) {
    }

    // apply any defaults
    $form->setDefaults($defaults);

    if (str_starts_with($formName, "CRM_Event_Form_Registration")) {
        $result = civicrm_api3('Event', 'get', [
            'sequential' => 1,
            'id' => $form->getVar('_eventId')
        ]);
        $event = $result['values'][0];

        $result = civicrm_api3('OptionValue', 'getvalue', [
            'return' => "label",
            'option_group_id' => "event_type",
            'value' => $event['event_type_id'],
        ]);
        $eventTypeLabel = $result;
        $isOnline = strpos($eventTypeLabel, "Online") !== false;
        $isCompanyShow = strpos($eventTypeLabel, "Company") !== false;

        if ($isCompanyShow) {
          $form->assign('directors', fetchParticipantsWithPreferredName($event['id'], "Director"));
          $form->assign('company_actors', fetchParticipantsWithPreferredName($event['id'], "Company Actor"));
          $form->assign('guest_actors', fetchParticipantsWithPreferredName($event['id'], "Guest Actor"));
          $form->assign('musical_improvisers', fetchParticipantsWithPreferredName($event['id'], "Musical Improviser"));
          $form->assign('tech_improvisers', fetchParticipantsWithPreferredName($event['id'], "Tech/Lighting Improviser"));
          $form->assign('show_graphic', $event['custom_488']);
          $form->assign('show_graphic_alt', $event['custom_489']);
          $form->assign('is_premium_show', $event['custom_495']);
        }

        try {
          $cancellationPolicy = civicrm_api3("Event", 'getvalue', array(
            'event_id' => $event['id'],
            'return' => 'custom_336'
          ));
        } catch (Exception $e) {
          $cancellationPolicy = ['is_error' => 1];
        }

        if ($cancellationPolicy['is_error'] == 0) {
          $form->assign('cancellation_policy', $cancellationPolicy);
        }

        $startDateTime = new DateTimeImmutable($event['start_date'], new DateTimeZone("America/Los_Angeles"));
        $interval = $startDateTime->diff(new DateTime('NOW'));

        if ($interval->format("%R") == '-') {
            if (abs($interval->format("%d")) < 1) {
                $timeAway = $interval->format("%h hours before");
            } else {
                $timeAway = $interval->format("%a days before");
            }
        } else {
            if (abs($interval->format("%d")) < 1) {
                $timeAway = $interval->format("%h hours after");
            } else {
                $timeAway = $interval->format("%a days after");
            }
        }

        // Add event info to tagmanager dataLayer
        Civi::resources()->addVars('analytics', [
            'civiEventType' => $eventTypeLabel,
            'civiEventIsOnline' => $isOnline,
            'civiEventTitle' => $event['event_title'],
            'civiEventTimeAway' => $timeAway,
            'civiEventId' => $event['id'],
        ]);
    }

    if ($formName == "CRM_Event_Form_Registration_Register") {
        // add event description to the registration page
        CRM_Core_Region::instance('page-body')->add(array(
            'template' => "{$templatePath}/EventDescriptionForRegistration.tpl"
        ));
    }

    if ($formName == "CRM_Event_Form_Registration_ThankYou" && $isOnline) {
        // show the zoom link when already registered for an online event
        CRM_Core_Region::instance('page-body')->add(array(
            'template' => "{$templatePath}/EventLink.tpl"
        ));

        CRM_Core_Region::instance('page-body')->add(array(
            'template' => "{$templatePath}/PostThankYou.tpl"
        ));
    }
}


/**
 * Customize various pages.
 */
function batsutils_civicrm_pageRun(&$page) {
    //\CRM_Core_Error::debug_log_message("Page ID is {$page->getVar("_id")}");
    $pageName = $page->getVar('_name');

    Civi::resources()->addScriptFile('batsutils', 'custom.js', 10, 'html-header');
    $session = CRM_Core_Session::singleton();
    $userId = $session->get('userID');
    Civi::resources()->addVars('analytics', [
        'civiUserId' => $userId,
        'civiUserLoggedIn' => isset($userId)
    ]);

    if ($pageName == "CRM_Event_Page_EventInfo") {
        $result = civicrm_api3('Event', 'get', [
            'sequential' => 1,
            'id' => $page->getVar('_id')
        ]);
        $event = $result['values'][0];

        if (!$event['is_public']) {
            CRM_Utils_System::permissionDenied();
            CRM_Utils_System::civiExit();
        }

        $result = civicrm_api3('OptionValue', 'getvalue', [
            'return' => "label",
            'option_group_id' => "event_type",
            'value' => $event['event_type_id'],
        ]);
        $eventTypeLabel = $result;
        $isOnline = strpos($eventTypeLabel, "Online") !== false;

        $params = [
                'contact_id' => $userId,
                'event_id' => $event['id'],
                'role_id' => $event['default_role_id']
        ];
        $isRegistered = CRM_Event_BAO_Event::checkRegistration($params);

        $startDateTime = new DateTimeImmutable($event['start_date'], new DateTimeZone("America/Los_Angeles"));
        $interval = $startDateTime->diff(new DateTime('NOW'));

        if ($interval->format("%R") == '-') {
            if (abs($interval->format("%d")) < 1) {
                $timeAway = $interval->format("%h hours before");
            } else {
                $timeAway = $interval->format("%a days before");
            }
        } else {
            if (abs($interval->format("%d")) < 1) {
                $timeAway = $interval->format("%h hours after");
            } else {
                $timeAway = $interval->format("%a days after");
            }
        }

        // Add event info to tagmanager dataLayer
        Civi::resources()->addVars('analytics', [
            'civiEventType' => $eventTypeLabel,
            'civiEventIsOnline' => $isOnline,
            'civiEventTitle' => $event['event_title'],
            'civiEventId' => $event['id'],
            'civiEventTimeAway' => $timeAway,
            'civiUserAlreadyRegistered' => $isRegistered
        ]);


        if ($isRegistered && $isOnline) {
            $templatePath = realpath(dirname(__FILE__)."/templates");
            CRM_Core_Region::instance('page-body')->add(array(
                'template' => "{$templatePath}/EventLink.tpl"
            ));
        }
    }
}

/**
 * "Send an Email" and "CiviMail" send different parameters to the tokenValues hook (in CiviCRM 5.x).
 * This works around that.
 *
 * @param array $tokens
 * @param string $group
 * @param string $token
 *
 * @return bool
 */
function batsutils_isTokenRequested($tokens, $group, $token) {
  \CRM_Core_Error::debug_log_message("in batsutils_isTokenRequested");
  // CiviMail sets $tokens to the format:
  //   [ 'group' => [ 'token_name' => 1 ] ]
  // "Send an email" sets $tokens to the format:
  //  [ 'group' => [ 0 => 'token_name' ] ]
  if (array_key_exists($token, $tokens[$group]) || in_array($token, $tokens[$group])) {
    return TRUE;
  }
  return FALSE;
}


/**
 * Add custom links
 */
//function batsutils_civicrm_searchTasks($objectType, &$tasks) {
//  CRM_Core_Error::debug_log_message("in batsutils_civicrm_searchTasks");
//  if ( $objectType == 'event' ) {
//      $tasks[600] = [
//           'title' => ts('Update multiple contacts'),
//           'class' => [
//             'CRM_Contact_Form_Task_PickProfile',
//             'CRM_Contact_Form_Task_Batch',
//           ],
//           'result' => TRUE,
//           'url' => 'civicrm/task/pick-profile',
//           'icon' => 'fa-pencil'
//      ];
//  }
//}

//function batsutils_civicrm_dupeQuery( $obj, $type, &$query ) {
//  \CRM_Core_Error::debug_log_message("in batsutils_civicrm_dupeQuery");
//  \CRM_Core_Error::debug_var("obj", $obj);
//  \CRM_Core_Error::debug_var("type", $type);
//  \CRM_Core_Error::debug_var("query", $query);
//  if ( $obj->title === 'Case Insensitive Name & Email' ) {
//     \CRM_Core_Error::debug_log_message("found case insensitive name + email!");
//    $query['civicrm_email.last_name.1'] = "SELECT t1.id id1, 1 weight
//        FROM civicrm_contact t1
//        WHERE t1.last_name LIKE \"{$obj->params['civicrm_contact']['last_name']}\"
//        AND t1.contact_type = 'Individual'";
//    $query['civicrm_email.first_name.1'] = "SELECT t1.id id1, 1 weight
//        FROM civicrm_contact t1
//        WHERE t1.first_name LIKE \"{$obj->params['civicrm_contact']['first_name']}\"
//        AND t1.contact_type = 'Individual'";
//  }
//  \CRM_Core_Error::debug_var("query", $query);
//}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 */
//function batsutils_civicrm_preProcess($formName, &$form) {
//
//}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
//function batsutils_civicrm_navigationMenu(&$menu) {
//  _batsutils_civix_insert_navigation_menu($menu, 'Mailings', array(
//    'label' => E::ts('New subliminal message'),
//    'name' => 'mailing_subliminal_message',
//    'url' => 'civicrm/mailing/subliminal',
//    'permission' => 'access CiviMail',
//    'operator' => 'OR',
//    'separator' => 0,
//  ));
//  _batsutils_civix_navigationMenu($menu);
//}
