<?php

class CRM_Batsutils_SendFreeShowPass extends CRM_Civirules_Action {
  const TEST_FORM_NODE_ID = 181;
  const INTRO_SURVEY_NODE_ID = 183;
  const CLASS_SURVEY_NODE_ID = 184;

  const EXPIRY_CALC = "last day of + 18 month";
  const GENERIC_MESSAGE_TEMPLATE = 192;

  const FREE_PASS_CODE_FIELD = 'custom_462';
  const FREE_PASS_EXPIRES_ON_FIELD = 'custom_463';
  const FREE_PASS_REASON_FIELD = 'custom_465';
  const FREE_PASS_SOURCE_FIELD = 'custom_466';

  const SCHOOL_CLASS_EVENT_ID_FIELD = 'custom_514';
  const SCHOOL_CLASS_TITLE_FIELD = 'custom_515';
  const SCHOOL_CLASS_NAME_LEVEL_FIELD = 'custom_523';
  const SCHOOL_CLASS_CODE_FIELD = 'custom_516';
  const SCHOOL_CLASS_START_DATE_FIELD = 'custom_517';

  const CLASS_LEVEL_FIELD = 'custom_3';
  const CLASS_CODE_FIELD = 'custom_4';

  // hard-coding the multiple choice options from Free Pass Reason
  const REASON_CLASS = 1;
  const REASON_SHOW = 2;
  const REASON_AT_WORK = 3;
  const REASON_DONATION = 5;
  const REASON_OTHER = 7;

  public function getExtraDataInputUrl($ruleActionId) {
    return FALSE;
  }

  public function processAction(CRM_Civirules_TriggerData_TriggerData $triggerData) {
    \CRM_Core_Error::debug_log_message("in processAction for CRM_Batsutils_SendFreeShowPass");
	$trigger = $triggerData->getTrigger();
	$triggerId = $trigger->getTriggerId();

    // calculate expiration
    $code = getRandomString(6);
    $expiresOn = date("Y-m-d", strtotime(self::EXPIRY_CALC));
    $expiresOnPretty = date("M d, Y", strtotime(self::EXPIRY_CALC));

	// set up message template
	$messageTemplateId = self::GENERIC_MESSAGE_TEMPLATE;
	$messageTemplateParams = [
      'id' => $messageTemplateId,
      'template_params' => [
          'code' => $code,
          'code_expires_on' => $expiresOnPretty,
      ],
      'from' => "BATS Improv <bats@improv.org>",
	];

	// set up customized aspects of action based on how it was triggered
	if ($triggerId == 99) {
		// webform submission trigger
		$nodeId = $triggerData->getNodeId();
		\CRM_Core_Error::debug_var("nodeId", $nodeId);

		switch ($nodeId) {
			case self::TEST_FORM_NODE_ID:
				$contactId = $triggerData->getEntityData('Contact')['id'];
				$contact = civicrm_api3('Contact', 'getsingle', [
						'id' => $contactId
				]);
                $freePassReason = self::REASON_OTHER;
				$freePassSource = "Test";
			break;

		    // student has taken a class and completed the survey
			case self::CLASS_SURVEY_NODE_ID:
			case self::INTRO_SURVEY_NODE_ID:
				// fetch survey activity
				$surveyId = $triggerData->getEntityData('Activity')['id'];
				$surveyActivity = civicrm_api3('Activity', 'getsingle', [
						'id' => $surveyId
				]);
				\CRM_Core_Error::debug_var("surveyActivity", $surveyActivity);
				$eventId = $surveyActivity[self::SCHOOL_CLASS_EVENT_ID_FIELD];
				$event = civicrm_api3('Event', 'getsingle', [
						'id' => $eventId
				]);

				// do some survey post-processing to copy class metadata to survey
				$result = civicrm_api3('Activity', 'create', [
						'id' => $surveyActivity['id'],
                        'subject' => "Student Survey for " . $event['title'],
						self::SCHOOL_CLASS_TITLE_FIELD => $event['title'],
						self::SCHOOL_CLASS_CODE_FIELD => $event[self::CLASS_CODE_FIELD],
						self::SCHOOL_CLASS_START_DATE_FIELD => $event['event_start_date'],
				]);
				\CRM_Core_Error::debug_var("result of updating survey", $result);

				// set up free pass info
				$contactId = $triggerData->getEntityData('Contact')['id'];
				$contact = civicrm_api3('Contact', 'getsingle', [
						'id' => $contactId
				]);
                $freePassReason = self::REASON_CLASS;
				$freePassSource = "Free Pass for " . $event['title'];
			break;

		}
	} else if ($triggerId == 94) {
		// event date reached trigger
		$participant = $triggerData->getEntityData('Participant');
		$contact = civicrm_api3('Contact', 'getsingle', [
				'id' => $participant['contact_id']
		]);
		$event = civicrm_api3('Event', 'getsingle', [
				'id' => $participant['event_id'],
		]);
	}

	// validate we have set up what we need
    if (!isset($contact) || !isset($freePassReason) || 
	 	!isset($freePassSource) || !isset($messageTemplateId)) {
		return;
    }

    // create activity
    $result = civicrm_api3('Activity', 'create', [
      'activity_type_id' => "Free Pass",
      'target_id' => $contact['id'],
      'source_contact_id' => $contact['id'],
      'subject' => $code,
      self::FREE_PASS_CODE_FIELD => $code,
      self::FREE_PASS_EXPIRES_ON_FIELD => $expiresOn,
      self::FREE_PASS_REASON_FIELD => $freePassReason,
      self::FREE_PASS_SOURCE_FIELD => $freePassSource
    ]);
    \CRM_Core_Error::debug_var("result", $result);


    // send free pass email
    // message template to use
    $messageTemplateParams['id'] = $messageTemplateId;

	// who to send to
    $messageTemplateParams['contact_id'] = $contact['id'];
    $messageTemplateParams['to_email'] = $contact['email'];
    $messageTemplateParams['to_name'] = $contact['display_name'];

	// custom template params
    $messageTemplateParams['template_params']['pass_reason'] = $freePassSource;

    $result = civicrm_api3('MessageTemplate', 'send', $messageTemplateParams);
    \CRM_Core_Error::debug_var("result of sending free pass email", $result);


	// update participant record to show free pass sent
	if (isset($participant)) {
        $result = civicrm_api3('Participant', 'create', [
            'id' => $participant['id'],
            'custom_494' => date("Y-m-d", strtotime("today"))
		]);
		\CRM_Core_Error::debug_var("result of updating participant", $result);
	}
  }
}

function getRandomString($n) {
  $characters = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
  $randomString = '';

  for ($i = 0; $i < $n; $i++) {
    $index = rand(0, strlen($characters) - 1);
    $randomString .= $characters[$index];
  }

  return $randomString;
}
