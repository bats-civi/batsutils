<?php

class CRM_Batsutils_SendFreePassAfterClientSurveyCompleted extends CRM_Civirules_Action {
  const ACTIVITY_FREE_PASS_CODE_FIELD = 'custom_409';
  const FREE_PASS_EMAIL_MESSAGE_TEMPLATE_ID = 154;

  public function getExtraDataInputUrl($ruleActionId) {
    return FALSE;
  }

  public function processAction(CRM_Civirules_TriggerData_TriggerData $triggerData) {
    $entityData = $triggerData->getAllEntityData();

    $basicActivity = $triggerData->getEntityData('Activity');

    $result = civicrm_api3('Activity', 'get', [
      'sequential' => 1,
      'return' => ["target_contact_id"],
      'id' => $basicActivity['id'],
     ]);
    $activity = $result['values'][0];

    $contactId = $activity['target_contact_id'][0];
    
    // TODO nicer/safer discount code algorithm
    $permitted_chars = '2345689ABCDEFGHJKMNPQRSTUVWXYZ';
    $code = 'W'.substr(str_shuffle($permitted_chars), 0, 6);
  
    $expirationDate = new DateTime('+18 months');
    $result = civicrm_api3('DiscountCode', 'create', [
      'code' => $code,
      'is_active' => 1,
      'amount' => 20,
      'amount_type' => 2,
      'count_max' => 1,
      'expire_on' => $expirationDate->format("Y-m-d"),
      'description' => "Free Pass (@ Work)",
      'source' => "@ Work Survey - Activity ID " . $activity['id'],
      'pricesets' => [79, 80, 81, 82, 87, 88],
      'filters' => "{\"event\":{\"event_type_id\":{\"IN\":[\"13\"]}}}"
    ]);

    // Add the discount code to the survey activity
    $result = civicrm_api3('Activity', 'create', [
      'id' => $activity['id'],
      self::ACTIVITY_FREE_PASS_CODE_FIELD => $code
    ]);

    // Send an email containing the discount code
    $result = civicrm_api3('Email', 'send', [
      'contact_id' => $contactId,
      'activity_id' => $activity['id'],
      'template_id' => self::FREE_PASS_EMAIL_MESSAGE_TEMPLATE_ID,
      'create_activity' => 1,
    ]);
  }
}
