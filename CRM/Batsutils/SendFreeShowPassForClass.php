<?php

class CRM_Batsutils_SendFreeShowPassForClass extends CRM_Civirules_Action {
  public function getExtraDataInputUrl($ruleActionId) {
    return FALSE;
  }

  public function processAction(CRM_Civirules_TriggerData_TriggerData $triggerData) {
    \CRM_Core_Error::debug_log_message("in processAction for CRM_Batsutils_SendFreeShowPassForClass");
    $participant = $triggerData->getEntityData('Participant');

    $contact = civicrm_api3('Contact', 'getsingle', [
      'id' => $participant['contact_id']
    ]);
    \CRM_Core_Error::debug_var("full contact", $contact);

    $event = civicrm_api3('Event', 'getsingle', [
      'id' => $participant['event_id'],
    ]);

    $expiresOn = date("Y-m-d", strtotime("last day of + 12 month"));
    $expiresOnPretty = date("M d, Y", strtotime("last day of + 12 month"));

    $code = getRandomString(6);
    $result = civicrm_api3('Activity', 'create', [
      'activity_type_id' => "Free Pass",
      'target_id' => $contact['id'],
      'source_contact_id' => $contact['id'],
      'subject' => $code,
      'custom_462' => $code,
      'custom_463' => $expiresOn,
      'custom_465' => 1, // student free pass
      'custom_466' => "Free Pass for " . $event['title']
    ]);
    \CRM_Core_Error::debug_var("result", $result);

    // send free pass email
    $result = civicrm_api3('MessageTemplate', 'send', [
      'id' => 176,
      'template_params' => [
          'code' => $code,
          'code_expires_on' => $expiresOnPretty,
          'class_title' => $event['title']
      ],
      'from' => "BATS Improv <bats@improv.org>",
      'contact_id' => $contact['id'],
      'to_email' => $contact['email'],
      'to_name' => $contact['display_name']
    ]);

    // update participant record to show free pass sent
    $result = civicrm_api3('Participant', 'create', [
        'id' => $participant['id'],
        'custom_494' => date("Y-m-d", strtotime("today"))
    ]);
  }
}

function getRandomString($n) {
  $characters = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
  $randomString = '';

  for ($i = 0; $i < $n; $i++) {
    $index = rand(0, strlen($characters) - 1);
    $randomString .= $characters[$index];
  }

  return $randomString;
}
