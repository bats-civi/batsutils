<?php

class CRM_Batsutils_ProcessDonationRequest extends CRM_Civirules_Action {
  const EXPIRY_CALC = "last day of + 18 month";
  const N_PASSES = 4;

  const FREE_PASS_CODE_FIELD = 'custom_462';
  const FREE_PASS_EXPIRES_ON_FIELD = 'custom_463';
  const FREE_PASS_REASON_FIELD = 'custom_465';
  const FREE_PASS_SOURCE_FIELD = 'custom_466';

  // hard-coding the multiple choice options from Free Pass Reason
  const REASON_CLASS = 1;
  const REASON_SHOW = 2;
  const REASON_AT_WORK = 3;
  const REASON_DONATION = 5;
  const REASON_OTHER = 7;

  public function getExtraDataInputUrl($ruleActionId) {
    return FALSE;
  }

  public function processAction(CRM_Civirules_TriggerData_TriggerData $triggerData) {
    \CRM_Core_Error::debug_log_message("in processAction for CRM_Batsutils_ProcessDonationRequest");

    $contactId = $triggerData->getEntityData('Contact')['id'];
    $contact = civicrm_api3('Contact', 'getsingle', [
      'id' => $contactId
    ]);
    \CRM_Core_Error::debug_var("contact", $contact);

    $expiresOn = date("Y-m-d", strtotime(self::EXPIRY_CALC));
    $expiresOnPretty = date("M d, Y", strtotime(self::EXPIRY_CALC));

    $codes = [];
    // create the free passes
    for ($i = 0; $i < self::N_PASSES; $i++) {
      $code = getRandomString(6);
      \CRM_Core_Error::debug_log_message("creating code $i - $code");
      $result = civicrm_api3('Activity', 'create', [
        'activity_type_id' => "Free Pass",
        'target_id' => $contact['id'],
        'source_contact_id' => $contact['id'],
        self::FREE_PASS_CODE_FIELD => $code,
        self::FREE_PASS_EXPIRES_ON_FIELD => $expiresOn,
        self::FREE_PASS_REASON_FIELD => self::REASON_DONATION,
        self::FREE_PASS_SOURCE_FIELD => "Donated to " . $contact['display_name']
      ]);
      $codes[] = $code;
      \CRM_Core_Error::debug_var("result of creating pass $i", $result);
    }

    $codesList = implode(", ", $codes);
    \CRM_Core_Error::debug_var("codesList", $codesList);

	// As a workaround to be able to email the codes and expiration date, copy
	// them to custom fields in the requesting organization's contact since we
	// can't access multiple activities from an email template
    $result = civicrm_api3('Contact', 'create', [
      'id' => $contact['id'],
      'custom_482' => $codesList,
      'custom_483' => $expiresOn,
      'custom_485' => date("Y-m-d") // last requested on this date
    ]);
    \CRM_Core_Error::debug_var("result", $result);

	// send email with codes and PDF attachment
    \CRM_Core_Error::debug_log_message("about to call PDF create");
    $result = civicrm_api3('Pdf', 'create', [
      'contact_id' => $contact['id'],
      'to_email' => $contact['email'],
      'template_id' => 173,
      'body_template_id' => 174,
      'email_subject' => "BATS Improv donated tickets for {$contact['display_name']}",
      'from_email' => "bats@improv.org",
      'from_name' => "BATS Improv",
      'email_activity' => 1,
      'pdf_activity' => 0,
      'bcc_email' => 'ana@improv.org'
    ]);
    \CRM_Core_Error::debug_var("result of calling PDF create", $result);
  }

}

function getRandomString($n) {
  $characters = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
  $randomString = '';

  for ($i = 0; $i < $n; $i++) {
    $index = rand(0, strlen($characters) - 1);
    $randomString .= $characters[$index];
  }

  return $randomString;
}
