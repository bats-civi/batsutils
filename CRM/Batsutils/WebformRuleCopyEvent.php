<?php

class CRM_Batsutils_WebformRuleCopyEvent extends CRM_Civirules_Action {
  public function getExtraDataInputUrl($ruleActionId) {
    return FALSE;
  }

  // Webform Node IDs
  const VRP_APPLICATION_WEBFORM_NODE_ID = 176;
  const SCHOLARSHIP_APPLICATION_WEBFORM_NODE_ID = 111;
  const NEW_CREDIT_FORM_NODE_ID = 62;
  const NEW_DISCOUNT_FORM_NODE_ID = 64;

  /*
	Custom Fields
  */
  const LIMIT_TO_CLASS_ID_FIELD = 'custom_202';
  const SCHOOL_CLASS_EVENT_ID_FIELD = 'custom_500';
  const SCHOOL_CLASS_EVENT_TITLE_FIELD = 'custom_501';

  // custom event fields
  const SLIDING_SCALE_FIELD = 'custom_333';
  const EVENTBRITE_TUITION_FIELD = 'custom_405';

  // scholarship application activity fields
  const SCHOLARSHIP_FOR_CLASS_ID_FIELD = 'custom_330';
  const SCHOLARSHIP_FOR_CLASS_TITLE_FIELD = 'custom_406';
  const AWARD_AMOUNT_FIELD = 'custom_335';
  const FUNCTION_AS_ACCESS_CODE_FIELD = 'custom_407';
  const EXPIRES_ON_FIELD = 'custom_408';

  public function processAction(CRM_Civirules_TriggerData_TriggerData $triggerData) {
    /*
		In order to offer a populated select list of events on a webform, we
		generate a fake "Temp" status event registration as part of the webform,
		then call a CiviRule to:
			(1) delete the fake registration
			(2) add the selected event ID (and other event info) to a custom field

	*/
    \CRM_Core_Error::debug_log_message("in processAction for CRM_Batsutils_WebformRuleCopyEvent");

    $nodeId = $triggerData->getNodeId();
    \CRM_Core_Error::debug_var("nodeId", $nodeId);

    $entityData = $triggerData->getAllEntityData();
    \CRM_Core_Error::debug_var("entityData", $entityData);

    $activity = $triggerData->getEntityData('Activity');
    $participant = $triggerData->getEntityData('Participant');


    if (empty($participant)) {
        \CRM_Core_Error::debug_log_message("participant not set, no event to copy for CRM_Batsutils_WebformRuleCopyEvent");
        return;
    }

    $pid = $participant['id'];
    $participant = civicrm_api3('Participant', 'getsingle', [
      'id' => $pid
    ]);
    \CRM_Core_Error::debug_var("participant", $participant);

    $event = civicrm_api3('Event', 'getsingle', [
      'id' => $participant['event_id']
    ]);
    \CRM_Core_Error::debug_var("event", $event);

    $contact = civicrm_api3('Contact', 'getsingle', [
      'id' => $participant['contact_id']
    ]);
    \CRM_Core_Error::debug_var("contact", $contact);

	// Update various things, depending on which form this is
	\CRM_Core_Error::debug_log_message("running custom update code depending on webform...");
	switch ($nodeId) {
		case self::NEW_CREDIT_FORM_NODE_ID:
		case self::NEW_DISCOUNT_FORM_NODE_ID:
			\CRM_Core_Error::debug_log_message("new credit/discount form");
			$result = civicrm_api3('Activity', 'create', [
					'id' => $activity['id'],
					self::LIMIT_TO_CLASS_ID_FIELD => $participant['event_id'],
			]);
			\CRM_Core_Error::debug_var("result of update activity", $result);
		break;

		case self::VRP_APPLICATION_WEBFORM_NODE_ID:
			\CRM_Core_Error::debug_log_message("VRP application form");
			$result = civicrm_api3('Activity', 'create', [
					'id' => $activity['id'],
					self::SCHOOL_CLASS_EVENT_ID_FIELD => $event['id'],
					self::SCHOOL_CLASS_EVENT_TITLE_FIELD => $event['title']
			]);
			\CRM_Core_Error::debug_var("result of update activity", $result);
		break;

		case self::SCHOLARSHIP_APPLICATION_WEBFORM_NODE_ID:
			// FIXME the eventbrite tuition is no longer being imported - not sure why, not a huge deal
		    $classTuitionCost = $event[self::EVENTBRITE_TUITION_FIELD];
		    $discountPercent = $activity[self::SLIDING_SCALE_FIELD]/100.0;
		    if ($classTuitionCost > 0) {
		    	$awardAmount = $classTuitionCost * $discountPercent;
		    }

		    if (strpos($event['title'], "Foundation 1") === false) {
		    	$accessCode = 1;
		    } else {
		    	$accessCode = 0;
		    }
		    $result = civicrm_api3('Activity', 'create', [
				'id' => $activity['id'],
				'subject' => "Scholarship Application from " . $contact['display_name'],
				self::SCHOLARSHIP_FOR_CLASS_ID_FIELD => $participant['event_id'],
				self::SCHOLARSHIP_FOR_CLASS_TITLE_FIELD => $event['title'],
				self::AWARD_AMOUNT_FIELD => $awardAmount,
				self::FUNCTION_AS_ACCESS_CODE_FIELD => $accessCode,
				self::EXPIRES_ON_FIELD => $event['start_date']
			]);
			\CRM_Core_Error::debug_var("result of activity update", $result);
		break;
	}

	// delete participant
    $result = civicrm_api3('Participant', 'delete', [
      'id' => $pid
    ]);
    \CRM_Core_Error::debug_var("result of delete participant", $result);
  }
}

