<?php

class CRM_Batsutils_ApprovedScholarshipApplicationCreateDiscount extends CRM_Civirules_Action {
  const DISCOUNT_ACTIVITY_TYPE = 89;
  const COC_SCHOLARSHIP_REASON = 105;
  const NEW_DISCOUNT = 1;

  const SCHOLARSHIP_FOR_CLASS_ID_FIELD = 'custom_330';
  const AWARD_AMOUNT_FIELD = 'custom_335';
  const FUNCITON_AS_ACCESS_CODE_APPLICATION_FIELD = 'custom_407';
  const EXPIRES_ON_FIELD = 'custom_408';

  const LIMIT_TO_CLASS_ID_FIELD = 'custom_202';
  const DISCOUNT_REASON_FIELD = 'custom_160';
  const CREDIT_AMOUNT_FIELD = 'custom_161';
  const STATUS_FIELD = 'custom_163';
  const AUTOMATICALLY_SEND_FIELD = 'custom_206';
  const FUNCTION_AS_ACCESS_CODE_FIELD = 'custom_203';
  const EXPIRATION_DATE_FIELD = 'custom_172';
  const ORIGIN_ID_FIELD = 'custom_173';
  const EMAIL_MESSAGE_TEMPLATE_FIELD = 'custom_418';
  const SCHOLARSHIP_MESSAGE_TEMPLATE = 126;

  public function getExtraDataInputUrl($ruleActionId) {
    return FALSE;
  }

  public function processAction(CRM_Civirules_TriggerData_TriggerData $triggerData) {
    \CRM_Core_Error::debug_log_message("in processAction for approved application");

    $contact = $triggerData->getEntityData('Contact');
    \CRM_Core_Error::debug_var("contact", $contact);

    // trigger data does not contain custom fields which we need
    $basicActivity = $triggerData->getEntityData('Activity');
    $result = civicrm_api3('Activity', 'get', [
      'sequential' => 1,
      'id' => $basicActivity['activity_id'],
    ]);
    $activity = $result['values'][0];

    // create a new Eventbrite discount activity based on the approved scholarship info
    $result = civicrm_api3('Activity', 'create', [
      'activity_type_id' => self::DISCOUNT_ACTIVITY_TYPE,
      'target_id' => $contact['id'],
      'subject' => "Scholarship for " . $contact['display_name'],
      self::DISCOUNT_REASON_FIELD => self::COC_SCHOLARSHIP_REASON,
      self::LIMIT_TO_CLASS_ID_FIELD => $activity[self::SCHOLARSHIP_FOR_CLASS_ID_FIELD],
      self::CREDIT_AMOUNT_FIELD => $activity[self::AWARD_AMOUNT_FIELD],
      self::FUNCTION_AS_ACCESS_CODE_FIELD => $activity[self::FUNCITON_AS_ACCESS_CODE_APPLICATION_FIELD],
      self::AUTOMATICALLY_SEND_FIELD => true,
      self::EMAIL_MESSAGE_TEMPLATE_FIELD => self::SCHOLARSHIP_MESSAGE_TEMPLATE,
      self::EXPIRATION_DATE_FIELD => $activity[self::EXPIRES_ON_FIELD],
      self::STATUS_FIELD => self::NEW_DISCOUNT,
      self::ORIGIN_ID_FIELD => "Civi " . $activity['id']
    ]);
    \CRM_Core_Error::debug_var("result of creating EB discount", $result);
  }
}
