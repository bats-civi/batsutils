{* Code to customize appearance of Event registration pages (for shows)  *}

<div id="event_info_description" style="">

{* add event description to registration page so they can work as stand-alone pages to reduce clicks-to-signup *}

{if $event.description}
<div class="crm-section event_description-section summary">
<img style="float:right; margin-left:10px;" src="{$show_graphic}" alt="{$show_graphic_alt}" />
<h2>{$event.event_start_date|date_format:"%A %b %e, %Y %l:%M %p"}</h2>
{$event.description|purify}
</div>

<p></p>


{* code to highlight specific show roles on registration page *}
{if $directors}
{if count($directors) == 1}
<h3>Director</h3>
{else}
<h3>Directors</h3>
{/if}
<ul style="list-style-type:none;">
{foreach from=$directors item="performer"}
<li>{$performer.preferred_name}</li>
{/foreach}
</ul>
{/if}

{if $company_actors}
<h3>Cast</h3>
<ul style="list-style-type:none;">
{foreach from=$company_actors item="performer"}
<li>{$performer.preferred_name}</li>
{/foreach}

  {if $guest_actors}
  {foreach from=$guest_actors item="performer"}
  <li>{$performer.preferred_name} (Guest)</li>
  {/foreach}
  {/if}

</ul><!-- end actor list -->
{/if}


{if $musical_improvisers}
<h3>Live Improvised Music by</h3>
<ul style="list-style-type:none;">
{foreach from=$musical_improvisers item="performer"}
<li>{$performer.preferred_name}</li>
{/foreach}
</ul>
{/if}

{if $tech_improvisers}
<h3>Live Improvised Lighting by</h3>
<ul style="list-style-type:none;">
{foreach from=$tech_improvisers item="performer"}
<li>{$performer.preferred_name}</li>
{/foreach}
</ul>
{/if}

<!-- end of event description section -->
<div style="clear:both;"></div>
{/if}



<hr />

<table class="form-layout">
  <tr><td>{ts}When{/ts}</td>
      <td width="90%">
        {$event.event_start_date|crmDate}
        {if $event.event_end_date}
            &nbsp; {ts}through{/ts} &nbsp;
            {* Only show end time if end date = start date *}
            {if $event.event_end_date|date_format:"%Y%m%d" == $event.event_start_date|date_format:"%Y%m%d"}
                {$event.event_end_date|crmDate:0:1}
            {else}
                {$event.event_end_date|crmDate}
            {/if}
        {/if}
      </td>
  </tr>

  {if $isShowLocation}
    {if $location.address.1}
      <tr><td>{ts}Location{/ts}</td>
          <td>
            {$location.address.1.display|nl2br}
            {if ( $event.is_map &&
            $config->mapProvider &&
      ( ( !empty($location.address.1.geo_code_1) && is_numeric($location.address.1.geo_code_1) )  ||
        ( !empty($location.address.1.city) AND !empty($location.address.1.state_province) ) ) ) }
              <br/><a href="{crmURL p='civicrm/contact/map/event' q="reset=1&eid=`$event.id`"}" title="{ts}Map this Address{/ts}" target="_blank">{ts}Map this Location{/ts}</a>
            {/if}
          </td>
      </tr>
    {/if}
  {/if}{*End of isShowLocation condition*}

</table>
<hr />

<p>With our new pricing structure, BATS no longer offers discount or promo codes. If you have a free pass with a unique code, please use <a href="/usemypass">this form to reserve your ticket for the show instead</a>.</p>
<p>Please contact bats@improv.org for assistance with registration or codes.</p>
<hr />

{if $cancellation_policy}
<p>Cancellation policy: {$cancellation_policy}</p>
<hr />
{/if}

</div>

<script type="text/javascript">
  cj('#event_info_description').insertBefore('.crm-event-register-form-block')
</script>
